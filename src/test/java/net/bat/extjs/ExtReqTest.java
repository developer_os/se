package net.bat.extjs;

import java.util.Map;

import net.bat.filter.ExtReq;

import org.junit.Test;
import org.springframework.data.domain.Sort;
import org.springside.modules.persistence.SearchFilter;

public class ExtReqTest {

	@Test
	public void test() throws Exception {
		String str_sort = "[{\"property\":\"objAuthor\",\"direction\":\"DESC\"}]";
		String str_filter = "[{\"operator\":\"eq\",\"value\":4,\"property\":\"objId\"},{\"operator\":\"like\",\"value\":\"7\",\"property\":\"objName\"},{\"operator\":\"like\",\"value\":\"6\",\"property\":\"objNo\"}]";
		ExtReq req = new ExtReq();
		Map<String, SearchFilter> mf = req.parseFilter(str_filter);
		Sort ms = req.parseSort(str_sort);
		System.out.println(ms);
	}
}
