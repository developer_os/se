package net.bat.dto;

import java.util.List;

public class ResultDTO<T> {

	public List<T> data;
	public long total;

}