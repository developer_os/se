package net.bat.filter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	public static final java.lang.String DATE_FORMAT = "yyyy-MM-dd";
	
	public static final java.lang.String DATE_FULL_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	public static boolean dateEquals(java.util.Date sj1, java.util.Date sj2) {
		if( sj1.equals(sj2))
			return true;
		return false;
	}

	public static boolean judgeIfHour(String hval) {
		if( null == hval || "".equals(hval) )
			return false;
		try {
			int hInt = Integer.parseInt(hval);
			if( hInt < 0 || hInt > 23 )
				return false;
		} catch (Throwable  e) {
			return false;
		}
		return true;
	}

	public static boolean judgeIfMinute(String mval) {
		if( null == mval || "".equals(mval) )
			return false;
		try {
			int mInt = Integer.parseInt(mval);
			if( mInt < 0 || mInt > 59 )
				return false;
		} catch (Throwable  e) {
			return false;
		}
		return true;
	}
	
	public static String getTimeString(java.util.Date sj, boolean hasfen) {
		// HH:mm:ss
		if( sj == null )
			return "";
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(sj);
		String hStr = String.valueOf(calDate.get(Calendar.HOUR_OF_DAY));
		String mStr = String.valueOf(calDate.get(Calendar.MINUTE));
		String sStr = String.valueOf(calDate.get(Calendar.SECOND));
		if( hasfen )
			return hStr+":"+mStr+":"+sStr;
		return hStr+":"+mStr;
	}
	  
	public static String getTimeChinese(java.util.Date sj) {
		// HH 时 mm 分
		if( sj == null )
			return "";
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(sj);
		String hStr = String.valueOf(calDate.get(Calendar.HOUR_OF_DAY));
		String mStr = String.valueOf(calDate.get(Calendar.MINUTE));
		return hStr+"时"+mStr+"分";
	}
	  
	public static String getTimeHour(java.util.Date sj, String defaultVal) {
		// 小时
		if( sj == null ) {
			if( defaultVal == null )
				return "";
			return defaultVal;
		}
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(sj);
		return String.valueOf(calDate.get(Calendar.HOUR_OF_DAY));
	}
	  
	public static String getTimeMinute(java.util.Date sj, String defaultVal) {
		// 分钟
		if( sj == null ) {
			if( defaultVal == null )
				return "";
			return defaultVal;
		}
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(sj);
		return String.valueOf(calDate.get(Calendar.MINUTE));
	}
	  
	public static String getDayString(java.util.Date sj) {
		// yyyy年MM月dd日
		if( sj == null )
			return "";
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(sj);
		String yStr = String.valueOf(calDate.get(Calendar.YEAR));
		String mStr = String.valueOf(calDate.get(Calendar.MONTH)+1);
		String rStr = String.valueOf(calDate.get(Calendar.DAY_OF_MONTH));
		return yStr+"年"+mStr+"月"+rStr+"日";
	}
	  
	public static String getDayStringForZrl(java.util.Date sj) {
		// yyyy年MM月dd日
		if( sj == null )
			return "";
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(sj);
		String yStr = String.valueOf(calDate.get(Calendar.YEAR));
		String mStr = String.valueOf(calDate.get(Calendar.MONTH)+1);
		String rStr = String.valueOf(calDate.get(Calendar.DAY_OF_MONTH));
		return yStr+"年<br>"+mStr+"月"+rStr+"日";
	}

	public static String getOracleDate0(java.util.Date sj) {
		// 将日期中的时间设置为当天的最早时间（得到 oracle 中 sql 语句中要的日期字符串）
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(sj);
		StringBuffer tmpStrbuf=new StringBuffer();
	  	tmpStrbuf.append("to_date('");
	  	tmpStrbuf.append(String.valueOf(calDate.get(Calendar.YEAR)));  
	  	tmpStrbuf.append("-");  
	  	tmpStrbuf.append(String.valueOf(calDate.get(Calendar.MONTH)+1));  
	  	tmpStrbuf.append("-");  
	  	tmpStrbuf.append(String.valueOf(calDate.get(Calendar.DAY_OF_MONTH)));  
	  	tmpStrbuf.append(" 0:0:0");  
	  	tmpStrbuf.append("','yyyy-mm-dd HH24:MI:SS')");  
		return tmpStrbuf.toString();
	}

	public static String getOracleDate24(java.util.Date sj) {
		// 将日期中的时间设置为当天的最晚时间（得到 oracle 中 sql 语句中要的日期字符串）
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(sj);
		StringBuffer tmpStrbuf=new StringBuffer();
	  	tmpStrbuf.append("to_date('");
	  	tmpStrbuf.append(String.valueOf(calDate.get(Calendar.YEAR)));  
	  	tmpStrbuf.append("-");  
	  	tmpStrbuf.append(String.valueOf(calDate.get(Calendar.MONTH)+1));  
	  	tmpStrbuf.append("-");  
	  	tmpStrbuf.append(String.valueOf(calDate.get(Calendar.DAY_OF_MONTH)));  
	  	tmpStrbuf.append(" 23:59:59");  
	  	tmpStrbuf.append("','yyyy-mm-dd HH24:MI:SS')");  
		return tmpStrbuf.toString();
	}
	
	// 根据 format 给出的格式将 str 转换为 Date 数据类型
	public static Date toDate(String str ,String format){
		if (str ==null ||"".equals(str)) return null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(str);
		} catch (Throwable  e) {
			return null;
		}
	}
	
	public static Calendar getCalendar(String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.getCalendar();
		} catch (Throwable  e) {
			return Calendar.getInstance();
		}
	}
	
	// 返回今天的日期型变量
	public static Date getToday(boolean hasTime) {
		if( hasTime ) {
			Date rtnDate = new Date();
			return rtnDate;
		} else // 不包含时间简单
			return new Date();
	}
	
	// 返回今天的日期型变量
	public static Date getYesterday() {
		Calendar calDate = getZhouCalendar();
		calDate.setTime(getToday(false));
		calDate.add(Calendar.DAY_OF_MONTH, 1);
		Date rtnDate = calDate.getTime();
		return rtnDate;
	}
	
	// 得到今年的年度（整形）
	public static int getNianDuInteger() {
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(getToday(false));
		return calDate.get(Calendar.YEAR);
	}
	
	// 得到今年的月份（整形）
	public static int getYueFenInteger() {
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(getToday(false));
		return calDate.get(Calendar.MONTH)+1;
	}
	
	// 得到今年的日子（整形）
	public static int getriziInteger() {
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(getToday(false));
		return calDate.get(Calendar.DAY_OF_MONTH);
	}

	// 得到指定日期的年度（整形）
	public static int getNianDuInteger(java.util.Date dDate) {
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(dDate);
		return calDate.get(Calendar.YEAR);
	}
	
	// 得到指定日期的月份（整形）
	public static int getYueFenInteger(java.util.Date dDate) {
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(dDate);
		return calDate.get(Calendar.MONTH)+1;
	}
	
	// 得到指定日期的日子（整形）
	public static int getriziInteger(java.util.Date dDate) {
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(dDate);
		return calDate.get(Calendar.DAY_OF_MONTH);
	}
	
	// 将日期变成整数
	public static int getDateToInteger(java.util.Date dDate) {
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(dDate);
		int tmpint = calDate.get(Calendar.YEAR) - 2000;
		tmpint = 100 * tmpint + calDate.get(Calendar.MONTH)+1;
		tmpint = 100 * tmpint + calDate.get(Calendar.DAY_OF_MONTH);
		return tmpint;
	}

	public static java.util.Date getFirstTimeOfYear(int nian) {
		// 指定年的最早时刻
		String dStr = String.valueOf(nian)+"-01-01 0:0:0";
		return toDate(dStr, "yyyy-MM-dd HH:mm:ss");
	}
	
	public static java.util.Date getEndTimeOfYear(int nian) {
		// 指定年的最晚时刻
		String dStr = String.valueOf(nian)+"-12-31 23:59:59";
		return toDate(dStr, "yyyy-MM-dd HH:mm:ss");
	}
	
	public static java.util.Date getQunianDate(java.util.Date dDate) {
		// 得到指定日期去年的对应日期。
		int nianInt = getNianDuInteger(dDate) - 1;
		int yueInt = getYueFenInteger(dDate);
		int riInt = getriziInteger(dDate);
		riInt = getRightDay(nianInt, yueInt, riInt);
	    StringBuffer strbuf=new StringBuffer();
	    strbuf.append(String.valueOf(nianInt));
	    strbuf.append("-");
	    strbuf.append(String.valueOf(yueInt));
	    strbuf.append("-");
	    strbuf.append(String.valueOf(riInt));
	    strbuf.append(" 0:0:0");
		return DateUtil.toDate(strbuf.toString(), "yyyy-MM-dd HH:mm:ss");
	}
	
	public static java.util.Date getTjRbDate(java.util.Date dDate, boolean isBefore) {
		// 得到统计日报的对应日期。
		int nianInt = getNianDuInteger(dDate);
		int yueInt = getYueFenInteger(dDate);
		int riInt = getriziInteger(dDate);
		return getDateByNianyueri(nianInt, yueInt, riInt, isBefore);
	}
	
	public static java.util.Date getTjXbDate(java.util.Date dDate, boolean isBefore) {
		// 得到统计旬报的对应日期。
		int nianInt = getNianDuInteger(dDate);
		int yueInt = getYueFenInteger(dDate);
		int riInt = getriziInteger(dDate);
	    if( isBefore ) {
	    	if( riInt <= 10 )
	    		return getDateByNianyueri(nianInt, yueInt, 1, isBefore);
	    	else if( riInt <= 20 )
	    		return getDateByNianyueri(nianInt, yueInt, 11, isBefore);
	    	else
	    		return getDateByNianyueri(nianInt, yueInt, 21, isBefore);
	    } else {
	    	if( riInt <= 10 )
	    		return getDateByNianyueri(nianInt, yueInt, 10, isBefore);
	    	else if( riInt <= 20 )
	    		return getDateByNianyueri(nianInt, yueInt, 20, isBefore);
	    	else
	    		riInt = 31;
	    }
		riInt = getRightDay(nianInt, yueInt, riInt);
		return getDateByNianyueri(nianInt, yueInt, riInt, isBefore);
	}
	
	public static java.util.Date getTjYbDate(java.util.Date dDate, boolean isBefore) {
		// 得到统计月报的对应日期。
		int nianInt = getNianDuInteger(dDate);
		int yueInt = getYueFenInteger(dDate);
		int riInt = getriziInteger(dDate);
	    if( isBefore ) {
    		riInt = 1;
	    } else {
    		riInt = 31;
	    }
		riInt = getRightDay(nianInt, yueInt, riInt);
		return getDateByNianyueri(nianInt, yueInt, riInt, isBefore);
	}
	
	public static java.util.Date getTjJbDate(java.util.Date dDate, boolean isBefore) {
		// 得到统计季报的对应日期。
		int nianInt = getNianDuInteger(dDate);
		int yueInt = getYueFenInteger(dDate);
		if( yueInt < 4) {
		    if( isBefore )
				return getDateByNianyueri(nianInt, 1, 1, isBefore);
		    else
				return getDateByNianyueri(nianInt, 3, 31, isBefore);
		} else if( yueInt < 7) {
		    if( isBefore )
				return getDateByNianyueri(nianInt, 4, 1, isBefore);
		    else
				return getDateByNianyueri(nianInt, 6, 30, isBefore);
		} else if( yueInt < 10) {
		    if( isBefore )
				return getDateByNianyueri(nianInt, 7, 1, isBefore);
		    else
				return getDateByNianyueri(nianInt, 9, 30, isBefore);
	    }
	    if( isBefore )
			return getDateByNianyueri(nianInt, 10, 1, isBefore);
		return getDateByNianyueri(nianInt, 12, 31, isBefore);
	}
	
	public static java.util.Date getTjNbDate(java.util.Date dDate, boolean isBefore) {
		// 得到统计年报的对应日期。
		int nianInt = getNianDuInteger(dDate);
	    if( isBefore )
			return getDateByNianyueri(nianInt, 1, 1, isBefore);
		return getDateByNianyueri(nianInt, 12, 31, isBefore);
	}
	
	public static java.util.Date getDataAddTime(java.util.Date dDate, String hval, String mval) {
		// 得到指定日期加上时间的日期
		int nianInt = getNianDuInteger(dDate);
		int yueInt = getYueFenInteger(dDate);
		int riInt = getriziInteger(dDate);
	    StringBuffer strbuf=new StringBuffer();
	    strbuf.append(String.valueOf(nianInt));
	    strbuf.append("-");
	    strbuf.append(String.valueOf(yueInt));
	    strbuf.append("-");
	    strbuf.append(String.valueOf(riInt));
	    strbuf.append(" ");
	    strbuf.append(hval);
	    strbuf.append(":");
	    strbuf.append(mval);
	    strbuf.append(":0");
		return DateUtil.toDate(strbuf.toString(), "yyyy-MM-dd HH:mm:ss");
	}
	
	public static String getDataAddTimeStr(java.util.Date dDate, String hval, String mval) {
		// 得到指定日期加上时间的日期
		int nianInt = getNianDuInteger(dDate);
		int yueInt = getYueFenInteger(dDate);
		int riInt = getriziInteger(dDate);
	    StringBuffer strbuf=new StringBuffer();
	    strbuf.append(String.valueOf(nianInt));
	    strbuf.append("-");
	    strbuf.append(String.valueOf(yueInt));
	    strbuf.append("-");
	    strbuf.append(String.valueOf(riInt));
	    strbuf.append(" ");
	    strbuf.append(hval);
	    strbuf.append(":");
	    strbuf.append(mval);
	    strbuf.append(":0");
		return strbuf.toString();
	}
	
	public static int getRightDay(int nian, int yue, int ri) {
		if( ri <= 28)
			return ri;
		if( yue == 2 ) {
			if( isRunNian(nian) )
				return 29;
			return 28;
		}
		if( yue == 1 || yue == 3 || yue == 5 || yue == 7 || yue == 8 || yue == 10 || yue == 12 )
			return ri;
		if( yue == 4 || yue == 6 || yue == 9 || yue == 11 ) {
			if( ri > 30)
				return 30;
			return ri;
		}
		return ri;
	}

	// 用车申请流程中归档时的日期时间整理函数
	public static java.util.Date getYcsqlcRqSj(java.util.Date day, String time) {
		// time：yyyy-MM-dd HH:mm:ss
		String tds[] = time.split(" ");
		String tmpt = time;
		if(tds.length == 2)
			tmpt = tds[1];
		String ts[] = tmpt.split(":");
		String hval = ts[0];
		String mval = ts[1];
		return getDataAddTime(day, hval, mval);
	}

	// ----------------------------------------------------------------
	// 以下这组函数用于关于一年中"周"的计算，共 5 个输出函数，其它都是私有的
	// 统一规定以下两条：
	//    1. 每年的第一周的周取值是：1；指定的周小于这个值，则取第一周。
	//    2. 跨年度的周算做前一年的最后一周。
	//    3. 如果指定的周大于今年的最后一周，则取今年的最后一周。
	// 例如：
	//	2007-12-29   年 = 2007   周 = 51
	//	2007-12-30   年 = 2007   周 = 52
	//	2007-12-31   年 = 2007   周 = 52
	//	2007 年第 52 周的第一天是：2007年12月30日，最后一天是：2008年1月5日
	//	2008-1-1   年 = 2007   周 = 52
	//	2008-1-5   年 = 2007   周 = 52
	//	2008-1-6   年 = 2008   周 = 1
	//	2008 年第 1 周的第一天是：2008年1月6日，最后一天是：2008年1月12日
	//	2008-12-27   年 = 2008   周 = 51
	//	2008-12-28   年 = 2008   周 = 52
	//	2008-12-31   年 = 2008   周 = 52
	//	2008 年第 52 周的第一天是：2008年12月28日，最后一天是：2009年1月3日
	//	2009-1-1   年 = 2008   周 = 52
	//	2009-1-3   年 = 2008   周 = 52
	//	2009-1-4   年 = 2009   周 = 1
	//	2009 年第 1 周的第一天是：2009年1月4日，最后一天是：2009年1月10日
	//	2011-12-24   年 = 2011   周 = 51
	//	2011-12-25   年 = 2011   周 = 52
	//	2011-12-31   年 = 2011   周 = 52
	//	2011 年第 52 周的第一天是：2011年12月25日，最后一天是：2011年12月31日
	//	2012-1-1   年 = 2012   周 = 1
	//	2012-1-7   年 = 2012   周 = 1
	//	2012-1-8   年 = 2012   周 = 2
	//	2012 年第 1 周的第一天是：2012年1月1日，最后一天是：2012年1月7日
	//	2012-12-29   年 = 2012   周 = 52
	//	2012-12-30   年 = 2012   周 = 53
	//	2012-12-31   年 = 2012   周 = 53
	//	2012 年第 52 周的第一天是：2012年12月23日，最后一天是：2012年12月29日
	//	2012 年第 53 周的第一天是：2012年12月30日，最后一天是：2013年1月5日
	//	2013-1-1   年 = 2012   周 = 53
	//	2013-1-5   年 = 2012   周 = 53
	//	2013-1-6   年 = 2013   周 = 1
	//	2013 年第 1 周的第一天是：2013年1月6日，最后一天是：2013年1月12日
	// ----------------------------------------------------------------
	private final static int firstDayOfWeek = Calendar.MONDAY;
	private final static int endDayOfWeek = Calendar.SUNDAY;
	private final static int endDayOfWorkWeek = Calendar.FRIDAY;
	private final static int minimalDaysInFirstWeek = 7; // 一年的第一周必须有7天
	// 得到指定周的第一天
	public static java.util.Date getFirstDayOfWeek(int nian, int zou) {
		int tmpzou = getReghtWeek(nian, zou);
		return getDateOfWeek(nian, tmpzou, firstDayOfWeek);
	}
	// 得到指定周的最后一天
	public static java.util.Date getEndDayOfWeek(int nian, int zou) {
		int tmpzou = getReghtWeek(nian, zou);
		return getDateOfWeek(nian, tmpzou, endDayOfWeek);
	}
	// 得到指定周的最后一个工作日
	public static java.util.Date getEndDayOfWorkWeek(int nian, int zou) {
		int tmpzou = getReghtWeek(nian, zou);
		return getDateOfWeek(nian, tmpzou, endDayOfWorkWeek);
	}
	// 得到指定周的指定星期几的日期
	public static java.util.Date getOneDayOfWeek(int nian, int zou, int xqj) {
		int tmpzou = getReghtWeek(nian, zou);
		return getDateOfWeek(nian, tmpzou, xqj);
	}
	// 得到指定日期的年数
	public static int getYearNoByDate(java.util.Date dDate) {
		Calendar calDate = getZhouCalendar();
		calDate.setTime(dDate);
		int nian = calDate.get(Calendar.YEAR);
		int yue = calDate.get(Calendar.MONTH);
		int zhou = calDate.get(Calendar.WEEK_OF_YEAR);
		if( yue == 0 && zhou > 50 )
			nian = nian - 1;
		return nian;
	}
	// 得到指定日期的周数
	public static int getWeekNoByDate(java.util.Date dDate) {
		Calendar calDate = getZhouCalendar();
		calDate.setTime(dDate);
		int zhou = calDate.get(Calendar.WEEK_OF_YEAR);
		return zhou;
	}
	// 得到今天的指定周偏移日期
	public static java.util.Date getDayOffset(int zhou) {
		Calendar calDate = getZhouCalendar();
		calDate.setTime(getToday(false));
		calDate.add(Calendar.WEEK_OF_YEAR, zhou);
		return calDate.getTime();
	}
	// 得到指定日期的指定周偏移日期
	public static java.util.Date getDayOffset(java.util.Date oldday, int zhou) {
		Calendar calDate = getZhouCalendar();
		calDate.setTime(oldday);
		calDate.add(Calendar.WEEK_OF_YEAR, zhou);
		return calDate.getTime();
	}
	// ---------------------------
	// 得到 Calendar，指定周的第一天是0；一年的第一周必须有7天
	private static Calendar getZhouCalendar() {
		Calendar calDate = Calendar.getInstance();
		calDate.setMinimalDaysInFirstWeek(minimalDaysInFirstWeek);
		calDate.setFirstDayOfWeek(firstDayOfWeek);
		return calDate;
	}
	// 统一为设置年、周、天计算 Calendar 类。
	private static Calendar getCalendar(int nian, int zou, int day) {
		Calendar calDate = getZhouCalendar();
		calDate.set(Calendar.YEAR, nian);
		calDate.set(Calendar.WEEK_OF_YEAR, zou);
		calDate.set(Calendar.DAY_OF_WEEK, day);
		return calDate;
	}
	// 得到今年总的周数
	private static int getWeekNumber(int nian) {
		Calendar calDate = getZhouCalendar();
		calDate.set(Calendar.YEAR, nian);
		calDate.set(Calendar.MONTH, 11);
		calDate.set(Calendar.DAY_OF_MONTH, 31);
		return calDate.get(Calendar.WEEK_OF_YEAR);
	}
	// 得到正确的周
	private static int getReghtWeek(int nian, int zou) {
		if( zou < 1)
			return 1;
		int maxzou = getWeekNumber(nian);
		if( zou > maxzou )
			return maxzou;
		return zou;
	}
	// 判断今年的第一天是否是元旦
	// 根据规则，如果今天是元旦，今年的周就是从“0”开始的，否则从“1”开始
	private static int getFirstWeekOfYear(int nian) {
//		Calendar calDate = getCalendar(nian, 0, 1);
//		if( calDate.get(Calendar.YEAR) == nian )
//			return 1;
		return 0;
	}
	// 得到指定周的指定天
	private static java.util.Date getDateOfWeek(int nian, int zou, int day) {
		int tmpzou = zou - getFirstWeekOfYear(nian);
		Calendar calDate = getCalendar(nian, tmpzou, day);
		return calDate.getTime();
	}
	// 以上这组函数用于关于一年中"周"的计算
	// ----------------------------------------------------------------

	// ----------------------------------------------------------------
	
	private static java.util.Date getDateByNianyueri(int nian, int yue, int ri, boolean isBefore) {
	    StringBuffer strbuf=new StringBuffer();
	    strbuf.append(String.valueOf(nian));
	    strbuf.append("-");
	    strbuf.append(String.valueOf(yue));
	    strbuf.append("-");
	    strbuf.append(String.valueOf(ri));
	    if( isBefore )
	    	strbuf.append(" 0:0:0");
	    else
	    	strbuf.append(" 23:59:59");
		return DateUtil.toDate(strbuf.toString(), "yyyy-MM-dd HH:mm:ss");
	}
	
	private static boolean isRunNian(int nian) {
		// 该函数计算当前年是否是闰年：四年一闰，百年不闰，四百年再闰
		if( nian % 4 != 0 )
			return false;
		if( nian % 100 == 0 ) {
			if( nian % 400 != 0 )
				return false;
		}
		return true;
	}
	
	/**通过date 和日期间隔天数   获得相应日期
	 * @param date 起始日期 Date类型
	 * @param dayNum     间隔天数 7 /-7  (结果：date+7  / date-7)     
	 * @param ifgl  是否归零 00:00:00 当天最早时间
	 * @return 如 Tue Dec 18 00:00:00 CST 2012(小时分钟秒均为零)        
	 */
	public static Date getDateByDayNum(Date date,int dayNum,boolean ifgl) {
        Calendar fromCalendar = Calendar.getInstance();   
        fromCalendar.setTime(date);   
        if(ifgl){
        	  fromCalendar.set(Calendar.HOUR_OF_DAY, 0);   
              fromCalendar.set(Calendar.MINUTE, 0);   
              fromCalendar.set(Calendar.SECOND, 0);   
              fromCalendar.set(Calendar.MILLISECOND, 0);
        }
        int day = fromCalendar.get(Calendar.DAY_OF_YEAR);
        fromCalendar.set(Calendar.DAY_OF_YEAR, day+dayNum);  
        return fromCalendar.getTime();
   }
	
	/**
	 * 获取指定格式的时间
	 *  AvensConstants.DATE_FORMAT
	 * @param dt
	 * @param format
	 * @return
	 */
	public static String getDate(Date dt, String format) {
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat(format);
		if (dt != null)
			return df.format(dt);
		else
			return "";
	}

	/**
	 * 
	 * @param dt
	 * @param format
	 * @return
	 */
	public static Date getDate(String dt, String format) {
		java.text.SimpleDateFormat df = new java.text.SimpleDateFormat(format);
		if (dt != null && !dt.trim().equals("")) {
			try {
				return df.parse(dt);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return null;
		} else
			return null;
	}

	
	/** 
     * 获取当年的第一天 
     * @param year 
     * @return 
     */  
    public static Date getCurrYearFirst(){  
        Calendar currCal=Calendar.getInstance();    
        int currentYear = currCal.get(Calendar.YEAR);  
        return getYearFirst(currentYear);  
    }  
      
    /** 
     * 获取当年的最后一天 
     * @param year 
     * @return 
     */  
    public static Date getCurrYearLast(){  
        Calendar currCal=Calendar.getInstance();    
        int currentYear = currCal.get(Calendar.YEAR);  
        return getYearLast(currentYear);  
    }  
      
    /** 
     * 获取某年第一天日期 
     * @param year 年份 
     * @return Date 
     */  
    public static Date getYearFirst(int year){  
        Calendar calendar = Calendar.getInstance();  
        calendar.clear();  
        calendar.set(Calendar.YEAR, year);  
        Date currYearFirst = calendar.getTime();  
        return currYearFirst;  
    }  
      
    /** 
     * 获取某年最后一天日期 
     * @param year 年份 
     * @return Date 
     */  
    public static Date getYearLast(int year){  
        Calendar calendar = Calendar.getInstance();  
        calendar.clear();  
        calendar.set(Calendar.YEAR, year);  
        calendar.roll(Calendar.DAY_OF_YEAR, -1);  
        Date currYearLast = calendar.getTime();  
          
        return currYearLast;  
        
    }
    }  