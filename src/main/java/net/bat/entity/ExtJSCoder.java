package net.bat.entity;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Date;

/**
 * 
 * @author c4w
 *         用于辅助生成前端Extjs代码
 *         用法:
 *         1.利用本工具,指定Class生成前端 model fields\grid columns\detail layout
 *         2.前端找一母本model,替换其名称和fields
 *         3.前端复制一view子目录
 *         分别替换grid columns\detail layout
 *         4.在nav tree中加入本grid id
 *         5.在Global.js中加入两句
 *         'XXXlist':{
 *         openobj:'onOpenObj'
 *         },
 *         'XXXdetail':{
 *         objnext:'objNext',
 *         objprev:'objPrev'
 *         },
 * 
 *         6.刷新界面&测试.
 *         7.细化:grid column和detail中的中文描述
 *         8.细化:grid中的filter
 *         9.细化:detail中的顺序,field type
 * 
 *
 */

public class ExtJSCoder {

	public static final String STR_SERIAL_VERSION_UID = "serialVersionUID";
	public static final String BIND_DEFAULT = "theObj";

	public static String printDetailFields(Class<?> cls, int cols, String bindTo, String[] fldNames) {
		StringBuffer sbuf = new StringBuffer();
		if (bindTo == null) {
			bindTo = BIND_DEFAULT;
		}
		Field[] flds;
		if (fldNames == null) {
			flds = cls.getDeclaredFields();
		} else {
			flds = new Field[fldNames.length];
			for (int i = 0; i < fldNames.length; i++) {
				try {
					flds[i] = cls.getDeclaredField(fldNames[i]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					flds[i] = null;
				}
			}
		}
		int pos = 0;
		int pos_col = 0;
		int pos_row = 0;
		for (Field fld : flds) {
			if ((fld == null) || fld.getName().equals(STR_SERIAL_VERSION_UID)) {
				continue;
			}
			if (pos_col == 0) {
				if (pos_row > 0) {
					sbuf.append(",");
				}
				sbuf.append("\n{\n" + "	xtype: 'container',\n" + "	layout: 'hbox',\n" + "	defaultType: 'textfield',\n"
						+ "	items:[\n");
				pos_row++;
			}
			if (pos_col > 0) {
				sbuf.append(",");
			}
			sbuf.append("	{\n" + "	flex: 1,\n" + "	fieldLabel: '" + fld.getName() + "',\n" + "	bind: '{" + bindTo + "."
					+ fld.getName() + "}'\n" + "	}");
			pos_col++;
			pos++;
			if (pos_col == cols) {
				sbuf.append("	]\n" + "}");
				pos_col = 0;
			}
		}
		// 非整除
		if (pos_col != 0) {
			sbuf.append("	]\n" + "}");

		}
		return sbuf.toString();
	}

	public static String printModelFields(Class<?> cls) {
		StringBuffer sbuf = new StringBuffer();
		Field[] flds = cls.getDeclaredFields();
		for (Field fld : flds) {
			String fldName = fld.getName();
			if (fldName.equals(STR_SERIAL_VERSION_UID)) {
				continue;
			}
			if (sbuf.length() > 0) {
				sbuf.append(",\n");
			}
			Type t = fld.getType();
			sbuf.append("{name:'" + fldName);
			if (t == String.class) {
				sbuf.append("'}");
			} else if (t == Date.class) {
				sbuf.append("', type: 'date'}");
			} else if (t == Boolean.class) {
				sbuf.append("', type: 'bool'}");
			} else {
				sbuf.append("', type: 'int'}");
			}
		}
		return sbuf.toString();
	}

	public static String printGridColumns(Class<?> cls) {
		StringBuffer sbuf = new StringBuffer();
		Field[] flds = cls.getDeclaredFields();
		for (Field fld : flds) {
			String fldName = fld.getName();
			if (fldName.equals(STR_SERIAL_VERSION_UID)) {
				continue;
			}
			if (sbuf.length() > 0) {
				sbuf.append(",\n");
			}
			Type t = fld.getType();
			sbuf.append("{text:'" + fldName + "',dataIndex:'" + fldName);
			if (t == String.class) {
				sbuf.append("',filter:'string'}");
			} else if (t == Date.class) {
				sbuf.append("', xtype: 'datecolumn',filter:true}");
			} else if (t == Boolean.class) {
				sbuf.append("', filter: 'boolean'}");
			} else {
				sbuf.append("', filter: 'number'}");
			}
		}
		return sbuf.toString();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Class cls = Task.class;
		System.out.println(printModelFields(cls));
		System.out.println("--------------------------");
		System.out.println(printGridColumns(cls));
		System.out.println("--------------------------");
		System.out.println(printDetailFields(cls, 2, null, null));
	}

}
